import random
import re
from string import ascii_lowercase as lowercase
from string import ascii_uppercase as uppercase
from string import digits
from string import punctuation

def make_password( length = 8, complexity = 1 ):
    '''Generate a random password with given length

    Allowed complexity levels:
        Complexity 1: only lowercase chars
        Complexity 2: Previous level plus at least 1 digit
        Complexity 3: Previous levels plus at least 1 uppercase char
        Complexity 4: Previous levels plus at least 1 punctuation char

    :param length: number of characters
    :param complexity: complexity level
    :returns: generated password
    '''
    try:
     
      if( ( length == 8 or length == 12 or length == 10) and  (complexity >= 4 or complexity >= 1 )):
        if (complexity == 1):
          return(''.join( random.choices(lowercase, k=length)))

        if (complexity == 2):
          return(''.join(random.choices(lowercase + digits , k=length))) 

        if (complexity == 3):
          return(''.join(random.choices(lowercase + digits + uppercase, k=length)))

        if (complexity == 4):
          return(''.join(random.choices(lowercase + digits + uppercase + punctuation, k=length)))

        else:
          return(''.join(random.choice(lowercase, k=length))) 
   
    except:
      
      print ("Please check your deeds ===>")
      raise

def check_password_level(password):
  '''Return the password complexity level

    Result levels:
        Complexity 1: If password has only lowercase chars
        Complexity 2: Previous level condition and at least 1 digit
        Complexity 3: Previous levels condition and at least 1 uppercase char
        Complexity 4: Previous levels condition and at least 1 punctuation

    Level exceptions:
        Complexity 2: password has length >= 8 chars and only lowercase chars
        Complexity 3: password has length >= 8 chars and only lowercase and digits

    :param password: password
    :returns: complexity level
  '''
  
  if(re.search(r'^[a-z]+$', password)):
    return 1

  if(re.search(r'^[a-z0-9]+$', password)):
    return 2

  elif((re.search(r'^[a-z0-9]+$', password)) and (len(password) >= 8)):
    return 2
  
  if(re.search(r'^\w+$', password)):
    return 3

  elif( (re.search(r'^\w+$', password)) and (len(password) >= 8)):
    return 3

  if(re.search(r'^[a-zA-Z0-9' + punctuation + ']*$' , password)):
    return 4
  