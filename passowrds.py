import random
from skeleton import make_password
from skeleton import check_password_level

def generate_random_passwords():
  index_one = 8
  for i in range(3):
    try: 
      with open('passwords.dat', 'a+') as file:
        file.write(make_password(index_one, 1) + '\n')
        file.write(make_password(index_one, 2) + '\n')
        file.write(make_password(index_one, 3) + '\n')
        file.write(make_password(index_one, 4) + '\n')
        index_one += 2
    except IOError as e:
      print(e.strerror)

def read_and_check_passwords():
  try:
    with open('passwords.dat', 'r') as file, open('complexity.dat', 'a+') as cplx:
      for temp in file:
        pwd =  open(str(random.random() * 100) + '.fml', 'w+')
        pwd.write(str(temp) + '\n')
        pwd.close()
        cplx.write(str(check_password_level(str(temp).strip())) + '\n')
  except IOError as e:
      print(e.strerror)


def main():
  generate_random_passwords()
  read_and_check_passwords() 

if __name__ == '__main__':
    main()

